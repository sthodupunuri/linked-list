#include <stdio.h>
#include <stdlib.h>
/* Defines a structure that is added and removed from the repository each time.
 * Structure has key, data, and a next pointer within it
 */
typedef struct dummy_element{
	int key;
	int val;
	struct dummy_element *next;
} element;

/* The first element of the repository will be empty but be useful for removing edge cases */
element r_head;


/* Global variables to hold the number of 'next' steps performed
 * and the size of the repository
 */
unsigned long int count=0;
int size=0;

/* This function initializes the repository and creates an empty first element */
int Repository_init()
{
	r_head.key = -1;
	r_head.val = 0;
	r_head.next = NULL;
	return 1;
}

/* Function to insert {key, data} pair to repository if the key is not in it already.
 * If key is in the repository, replaces its data with the new data and returns 0. If the 
 * pair is added, returns 1. If an error occurs, returns -1
 */
int Repository_update(int key, int data)
{
	/* Initiate pointers to point to the current element and to the element that will be later added */
	element *temp_pointer;
	element *pelement;

	/* Point to the first element of the repository that will serve as its head */
	temp_pointer = &r_head;
	
	/* Loop through repository until either the key is found or a number greater than the one
	 * being searched for is found. If the key is found, it's data is updated. If it is not found
	 * it is added to the repository.
     */
	while( (temp_pointer->next)!=NULL && key >= (temp_pointer->next)->key){
		/* Change key's data if it is found in the repository */
		if((temp_pointer->next)->key == key){
			(temp_pointer->next)->val = data;
			return 0;
		} 
		
		temp_pointer = temp_pointer->next;
		count++;
	}
	
	/* This code is ran if the key was not found in the repository, so it is added to the repository */
	pelement = malloc(sizeof(element));
	if( pelement == NULL ) return -1;
	pelement->val = data;
	pelement->key = key;
	pelement->next = temp_pointer->next;
	count++;
	temp_pointer->next = pelement;
	count++;
	size++;
	return 1;
}

/* A function to delete a key, value pair from the repository. Returns 0
 * if key not found in repository. Returns 1 if key was successfully deleted.
 * Returns -1 if an error occurs
 */
int Repository_delete(int key){
	/* Initialize 2 pointers. One to point to the current element of the list
	 * and another to point to the element being deleted (if needed)
	 */
	element *temp_pointer;
	element *temp_pointer2;

	/* Point to the first element of the repository that will serve as its head */
	temp_pointer = &r_head;
	

	/* Loops through repository until a value greater than the key is found or the key is found.
     * If the key is found, its associated element is removed. If it is not found, 0 is returned.
     */
	while(temp_pointer->next != NULL && (key >= (temp_pointer->next)->key) ){
		if( (temp_pointer->next)->key == key){
			temp_pointer2 = temp_pointer->next;
			count++;
			temp_pointer->next = (temp_pointer->next)->next;

			free(temp_pointer2);
			size--;
			return 1;
		}

		temp_pointer = temp_pointer->next;
		count++;
	}
	return 0;

}

/* This function returns the data associated with a key if it is found in the 
 * repository. It returns this data to the variable associated with the pointer inputted. 
 * If the key is not found, returns 0.
 */
int Repository_get(int key,int *data){
	element *temp_pointer;
	
	temp_pointer = &r_head;

	/* Loops through the repository until either the key or a value greater than the key is found 
     */
	while((temp_pointer->next)!= NULL && (key >= (temp_pointer->next)->key) ){
		
		if((temp_pointer->next)->key == key){
			*data = (temp_pointer->next)->val;
			return 1;
		}
	
		temp_pointer = temp_pointer->next;
		count++;	
	}
	
	/* If the end of the repository is found, or a value greater than the key is found,
	 * then the key is not in the repository and 0 is returned.
     */ 
	return 0;	
}


/* A function to print the size of the repository and the number of 'next' steps performed.
 * Depending on the value of the input, the {key, data} pairs can also be printed
 */
void Repository_print(int print_elements){
	element *temp_pointer;
	temp_pointer = &r_head;

	printf("Sorted repository has %u elements with %lu steps performed",size,count);
	
	/* If the input indicates 'yes' the {key, data} pairs are printed */	
	if( print_elements == 1 ){
		printf("\nElements:  ");

		/* Loops through the repository and prints all the keys and associated data */
		while( temp_pointer->next!=NULL){
			printf("{%d, %d} ",(temp_pointer->next)->key, (temp_pointer->next)->val);
			temp_pointer = temp_pointer->next;
		}
	}
	printf("\n--------------\n");
}
