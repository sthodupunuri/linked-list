CC = gcc
CFLAGS = -c -ansi -g

all: project2 clean

project2: 	linked_list.o repository.o
		$(CC) -o linked_list linked_list.o repository.o

clean:
		rm -f *.o

%.o:	%.c
		$(CC) $(CFLAGS) $*.c
