# Linked-List
Holds the files required to make a sorted linked-list repository that can  add <key, data> pairs to it. 
Contains functions to add, remove, get, and print the contents of the linked list.
